package com.brad.test.utils.selenium;

import com.brad.test.framework.PortableTestParams;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.relevantcodes.extentreports.LogStatus;

public abstract class InputMethods extends ClickMethods {

	public InputMethods(WebDriver dr, PortableTestParams ptm) {
		super(dr, ptm);
	}

	/**
	 * Send the specified input to the specified element
	 * @param by
	 * @param input
	 */
	public void sendInput(By by, String input) {
		if (input == null)
			return;
		sendInput(findElement(by), by, input);
	}

	/**
	 * Send the specified input to the specified element
	 * @param element
	 * @param input
	 */
	public void sendInput(WebElement element, String input) {
		if (input == null)
			return;
		sendInput(element, null, input);
	}

	/**
	 * Send the specified input to the specified element and use the specified InputActions
	 * @param by
	 * @param input
	 */
	public void sendInput(By by, String input, InputAction... actions) {
		if (input == null)
			return;
		sendInput(findElement(by), by, input, actions);
	}

	/**
	 * Send the specified input to the specified element and use the specified InputActions
	 * @param element
	 * @param input
	 */
	public void sendInput(WebElement element, String input, InputAction... actions) {
		if (input == null)
			return;
		sendInput(element, null, input, actions);
	}

	private void sendInput(WebElement element, By by, String input, InputAction... actions) {
		if (input == null)
			return;

		if (Boolean.valueOf(System.getProperty("alwaysClearElementBeforeInput")))
			clear(element);
		else
			handleInputAction(element, actions, InputAction.CLEAR);
		handleInputAction(element, actions, InputAction.REQUEST_FOCUS);

		if (getBrowser().equals("IE")) {
			for (char c : input.toCharArray()) {
				element.sendKeys(String.valueOf(c));
				sleep(5);
			}
		} else {
			element.sendKeys(input);
		}

		handleInputAction(element, actions, InputAction.SEND_ENTER_KEY);

		log(LogStatus.INFO,
				"Sending input <b>" + input + "</b> to " + (by != null ? "<b>" + by.toString() + "</b>" : "element"));
		takeScreenshot(by, element);
	}

	private void clear(WebElement element) {
		log(LogStatus.INFO, "Clearing");
		try {
			element.clear();
		} catch (Exception e) {
			click(element, ClickType.NORMAL);
			sleep(1500);
			if (getOs().contains("Mac"))
				element.sendKeys(Keys.chord(Keys.COMMAND, "a"));
			else
				element.sendKeys(Keys.chord(Keys.CONTROL, "a"));
			element.sendKeys(Keys.DELETE);
			click(element, ClickType.NORMAL);
		}
	}

	private void handleInputAction(WebElement element, InputAction[] actions, InputAction handle) {
		if (actions == null)
			return;
		for (InputAction action : actions) {
			if (action.equals(handle)) {
				switch (action) {
				case CLEAR:
					clear(element);
					break;
				case REQUEST_FOCUS:
					click(element, ClickType.NORMAL);
					break;
				case SEND_ENTER_KEY:
					element.sendKeys(getOs().contains("Mac") ? Keys.RETURN : Keys.ENTER);
					break;
				}
			}
		}
	}

	public enum InputAction {

		CLEAR, SEND_ENTER_KEY, REQUEST_FOCUS;

	}
}
