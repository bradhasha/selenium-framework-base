package com.brad.test.utils.selenium;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import com.brad.test.framework.PortableTestParams;
import org.apache.commons.lang.math.RandomUtils;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.By.ById;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import com.brad.test.framework.reporting.ExtentReportUtils;
import com.relevantcodes.extentreports.LogStatus;

public abstract class GetMethods {

	private WebDriver dr;
	private PortableTestParams ptm;
	private boolean logging = true;
	private int retryCount = 2;
	private int timeBetweenRetries = 60;

	public GetMethods(WebDriver dr, PortableTestParams ptm) {
		this.dr = dr;
		this.ptm = ptm;
		setDefaultRetryCount();
		setDefaultTimeBetweenRetries();
	}

	public void setDefaultRetryCount() {
		String rtc = System.getProperty("defaultRetryCount");
		if (rtc != null && rtc.length() > 0)
			this.retryCount = Integer.valueOf(rtc);
	}

	public void setDefaultTimeBetweenRetries() {
		String tbr = System.getProperty("timeBetweenRetries");
		if (tbr != null && tbr.length() > 0)
			this.timeBetweenRetries = Integer.valueOf(tbr);
	}

	/**
	 * Get the instance of WebDriver being used
	 * 
	 * @return dr
	 */
	public WebDriver getDriver() {
		return this.dr;
	}

	/**
	 * Get the instance of PortableTestParams being used
	 * 
	 * @return ptm
	 */
	public PortableTestParams getPtm() {
		return this.ptm;
	}

	/**
	 * Return true if logging is on, false if off
	 * 
	 * @return logging
	 */
	public boolean getLogging() {
		return this.logging;
	}

	/**
	 * Set logging on (true), or off (false)
	 * 
	 * @param bool
	 */
	public void setLogging(boolean bool) {
		this.logging = bool;
	}

	/**
	 * Get the retryCount loaded in from pom.xml
	 * 
	 * @return
	 */
	public int getDefaultRetryCount() {
		return this.retryCount;
	}

	/**
	 * Set the retryCount during runtime
	 * 
	 * @param retryCount
	 */
	public void setDefaultRetryCount(int retryCount) {
		this.retryCount = retryCount;
	}

	/**
	 * Get the timeBetweenRetries loaded in from pom.xml
	 * 
	 * @return
	 */
	public int getTimeBetweenRetries() {
		return this.timeBetweenRetries;
	}

	/**
	 * Set the time between retries during runtime
	 * 
	 * @param timeBetweenRetries
	 */
	public void setDefaultTimeBetweenRetries(int timeBetweenRetries) {
		this.timeBetweenRetries = timeBetweenRetries;
	}

	/**
	 * Get the operating system being used
	 * 
	 * @return os
	 */
	public String getOs() {
		return System.getProperty("os.name");
	}

	/**
	 * Get the browser being used
	 * 
	 * @return browser
	 */
	public String getBrowser() {
		return getPtm().getBrowserInfo().getBrowser();
	}

	/**
	 * Return a new instance of WebDriverWait to use with expected conditions
	 * 
	 * @param seconds
	 * @return WebDriverWait
	 */
	public WebDriverWait getWebDriverWait(int seconds) {
		return new WebDriverWait(getDriver(), seconds);
	}

	/**
	 * Get the specified element's rectangle outline
	 * 
	 * @param by
	 * @return Rectangle
	 */
	public Rectangle getRectangle(By by) {
		WebElement ele = findElement(by);
		return new Rectangle(ele.getLocation(), ele.getSize());
	}

	/**
	 * Get the specified element's rectangle outline
	 * 
	 * @param ele
	 * @return Rectangle
	 */
	public Rectangle getRectangle(WebElement ele) {
		try {
			return new Rectangle(ele.getLocation(), ele.getSize());
		} catch (Exception e) {
		}
		return null;
	}

	/**
	 * Get the raw selector from By.toString();
	 * 
	 * @param by
	 * @return raw selector
	 */
	public String getRawSelector(By by) {
		String sel = by.toString();
		return sel.substring(sel.indexOf(" ") + 1, sel.length());
	}

	/**
	 * Log output to the test report
	 * 
	 * @param status
	 * @param msg
	 */
	public void log(LogStatus status, String msg) {
		ExtentReportUtils.log(status, msg);
	}

	/**
	 * Take a screenshot of the specified element
	 * 
	 * @param by
	 * @param ele
	 */
	public void takeScreenshot(By by, WebElement ele) {
		ExtentReportUtils.takeAndInsertScreenshot(dr, by, ele != null ? getRectangle(ele) : null);
	}

	/**
	 * Find and return the specified element
	 * 
	 * @param by
	 * @return WebElement
	 */
	public WebElement findElement(By by) {
		return findElement(by, true);
	}

	/**
	 * Find and return the specified element, specify if is required that the
	 * element be located to continue
	 * 
	 * @param by
	 * @return WebElement
	 */
	public WebElement findElement(By by, boolean required) {
		WebElement element = null;
		int count = 0;

		while (count <= getDefaultRetryCount()) {

			try {
				element = getWebDriverWait(getTimeBetweenRetries()).ignoring(StaleElementReferenceException.class)
						.until(ExpectedConditions.presenceOfElementLocated(by));
				break;
			} catch (Exception e) {

				if ((count + 1) > retryCount) {
					if (required) {
						log(LogStatus.FAIL, "Error locating element <b>" + by.toString() + "</b>. Stopping test.");
						takeScreenshot(by, null);
						Assert.fail();
					} else {
						log(LogStatus.INFO,
								"Error locating element" + (by != null ? " <b>" + by.toString() + "</b>." : ".")
										+ " However, locating element was not required.  Continuing.");
						break;
					}
				} else {
					log(LogStatus.INFO,
							"Error locating element <b>" + by.toString() + "</b>.  Retrying Attempt #" + (count + 1));
					count++;
				}

			}
		}

		log(LogStatus.INFO, "Successfully located element <b>" + by.toString() + "</b>");
		takeScreenshot(by, element);
		return element;
	}

	/**
	 * Find and return a list of WebElements matching the specified selector
	 * 
	 * @param by
	 * @return list of WebElements
	 */
	public List<WebElement> findElements(By by) {
		return dr.findElements(by);
	}

	/**
	 * Get an instance of JavascriptExecutor using the current WebDriver
	 * 
	 * @return JavascriptExecutor
	 */
	public JavascriptExecutor getJsExecutor() {
		return (JavascriptExecutor) dr;
	}

	/**
	 * Loads in test data from the test data spreadsheet. Column A should be
	 * keys, and Column B should contain each keys corresponding value.
	 * 
	 * @param workbookFile
	 *            The name of the excel workbook. Example: test-data.xlsx
	 * @param worksheetName
	 *            The name of the worksheet inside of the excel workbook that
	 *            data should be loaded from. Best practice is to have one
	 *            worksheet per test.
	 * @return hashmap of data loaded in from excel workbook.
	 */
	public static HashMap<String, String> getTestData(String workbookFile, String worksheetName) {

		HashMap<String, String> data = new HashMap<>();
		DataFormatter formatter = new DataFormatter();
		XSSFWorkbook workbook = null;
		OPCPackage opcPackage = null;

		try {
			File file = new File(workbookFile);
			opcPackage = OPCPackage.open(file, PackageAccess.READ);
			workbook = new XSSFWorkbook(opcPackage);
			XSSFSheet sheet = workbook.getSheet(worksheetName);

			Iterator<Row> rowIterator = sheet.iterator();

			// Iterate through rows
			while (rowIterator.hasNext()) {
				Iterator<Cell> cellIterator = rowIterator.next().cellIterator();
				while (cellIterator.hasNext()) {
					String next1 = formatter.formatCellValue(cellIterator.next());
					String next2 = formatter.formatCellValue(cellIterator.next());
					System.out.print("Key: " + next1 + " -> Value: " + next2);
					data.put(next1, next2);
				}
				System.out.println();
			}

			// Close excel workbook
			if (workbook != null)
				workbook.close();

			// Close opcPackage opener
			if (opcPackage != null)
				opcPackage.close();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return data;
	}

	/**
	 * Return a random cell of data from a row
	 * 
	 * @param data
	 *            The hashmap of data being used if applicable. This will ensure
	 *            the same value is not stored twice in the map.
	 * @param workbookFile
	 *            The name of the excel workbook. Example: test-data.xlsx
	 * @param worksheetName
	 *            The name of the worksheet inside of the excel workbook that
	 *            data should be loaded from. Best practice is to have one
	 *            worksheet per test.
	 * @param column
	 *            The name of the column (note: columns are inverse meaning the
	 *            first column's rows are columns, and all columns to the right
	 *            are (rows).
	 * @return random row of data
	 */
	public static String getRandomRowOfDataFromColumnExcel(HashMap<String, String> data, String workbookFile,
			String worksheetName, String column) {

		XSSFWorkbook workbook = null;
		String randCellValue = null;
		OPCPackage opcPackage = null;

		try {
			File file = new File(workbookFile);
			opcPackage = OPCPackage.open(file, PackageAccess.READ);
			workbook = new XSSFWorkbook(opcPackage);
			XSSFSheet sheet = workbook.getSheet(worksheetName);

			// Iterate through rows
			Iterator<Row> rowIterator = sheet.iterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();

				if (row.getCell(0).getStringCellValue().equalsIgnoreCase(column)) {
					randCellValue = row.getCell(RandomUtils.nextInt(row.getLastCellNum()))
							.getStringCellValue();

					if (data != null) {
						while (existsInMap(data, randCellValue)) {
							randCellValue = row.getCell(RandomUtils.nextInt(row.getLastCellNum()))
									.getStringCellValue();
						}
					}
					break;
				}
			}

			// Close excel workbook
			if (workbook != null)
				workbook.close();

			// Close opcPackage opener
			if (opcPackage != null)
				opcPackage.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return randCellValue;
	}

	private static boolean existsInMap(HashMap<String, String> data, String checkValue) {
		for (String key : data.keySet()) {
			if (data.get(key).equalsIgnoreCase(checkValue))
				return true;
		}
		return false;
	}

	/**
	 * Static sleep for specified number of milliseconds.
	 * 
	 * @param milliseconds
	 */
	public void sleep(int milliseconds) {
		try {
			Thread.sleep(milliseconds);
			System.out.println("Sleeping for " + (String.valueOf((milliseconds) / 1000)) + " second(s)");
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Get text from the specified element using selenium's getText() method
	 * 
	 * @param by
	 * @return text
	 */
	public String getText(By by) {
		String elementText = null;
		try {

			elementText = findElement(by).getText();

		} catch (Exception e) {

			if (getLogging()) {
				log(LogStatus.FAIL,
						"Error checking getting text from " + by.toString() + " + using selenium getText().");
				takeScreenshot(by, null);
			}
			Assert.fail(e.getMessage());
		}

		return elementText;
	}

	/**
	 * Get text from the specified angularJS element using a custom method
	 * 
	 * @param by
	 * @return text
	 */
	public String getAngularJsText(By by) {
		String elementText = null;
		final String selector = by.toString().split(": ")[1];

		try {

			final String getAngularTextScript = "return angular.element("
					+ (by instanceof ById ? "'input#" + selector + "'"
							: "document.evaluate(\"" + selector
									+ "\", document, null, XPathResult.ANY_TYPE, null).iterateNext()")
					+ ")[0].value;";
			elementText = (String) getJsExecutor().executeScript(getAngularTextScript);
		} catch (Exception e) {

			if (getLogging()) {
				log(LogStatus.FAIL,
						"Error checking getting text from " + by.toString() + " + using angular js object.");
				takeScreenshot(by, null);
			}
			Assert.fail(e.getMessage());
		}

		return elementText;
	}

}
