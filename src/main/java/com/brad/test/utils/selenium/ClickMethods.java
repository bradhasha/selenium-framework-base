package com.brad.test.utils.selenium;

import java.util.concurrent.TimeUnit;

import com.brad.test.framework.PortableTestParams;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

public abstract class ClickMethods extends CheckMethods {

	public ClickMethods(WebDriver dr, PortableTestParams ptm) {
		super(dr, ptm);
	}

	/**
	 * Clicks the element using the specified ClickType
	 * 
	 * @param by
	 * @param type
	 */
	public void click(By by, ClickType type) {
		click(findElement(by), by, type, EvaluateExpected.EXISTS_CONDITION_REQUIRED);
	}

	/**
	 * Clicks the element using the specified ClickType
	 * 
	 * @param element
	 * @param type
	 */
	public void click(WebElement element, ClickType type) {
		click(element, null, type, EvaluateExpected.EXISTS_CONDITION_REQUIRED);
	}

	private void click(WebElement element, By by, ClickType type, final EvaluateExpected eval) {
		int count = 0;

		while (count <= getDefaultRetryCount()) {

			if (count > 0) {
				log(LogStatus.INFO, "Pre-retry #" + count + " screenshot.");
				takeScreenshot(by, element);
			}

			try {
				getWebDriverWait(20).until(ExpectedConditions.elementToBeClickable(element));
				type.perform(getJsExecutor(), element);

				if (getLogging())
					log(LogStatus.INFO,
							"Successfully clicked element " + (by != null ? "<b>" + by.toString() + "</b>." : "."));
				break;
			} catch (Exception e) {

				if ((count + 1) > getDefaultRetryCount()) {
					if (!eval.name().contains("NOT_REQUIRED")) {
						log(LogStatus.FAIL, "Error clicking element"
								+ (by != null ? " <b>" + by.toString() + "</b>." : ".") + " Stopping test.");
						takeScreenshot(by, null);
						Assert.fail();
					} else {
						log(LogStatus.INFO,
								"Error clicking element" + (by != null ? " <b>" + by.toString() + "</b>." : ".")
										+ " However, click was not required.  Continuing.");
						break;
					}
				} else {
					log(LogStatus.INFO, "Error clicking element" + (by != null ? " <b>" + by.toString() + "</b>." : ".")
							+ " Retrying Attempt #" + (count + 1));
					count++;
					element = findElement(by);
				}

			}
		}
	}

	/**
	 * Clicks the element using the specified ClickType and then checks that the
	 * expected element appears.
	 * 
	 * @param by
	 * @param type
	 * @param expected
	 */
	public void click(By by, ClickType type, By expected) {
		click(findElement(by), by, type, expected, EvaluateExpected.EXISTS_CONDITION_REQUIRED);
	}

	/**
	 * Clicks the element using the specified ClickType and then checks that the
	 * expected element appears.
	 * 
	 * @param element
	 * @param type
	 * @param expected
	 */
	public void click(WebElement element, ClickType type, By expected) {
		click(element, null, type, expected, EvaluateExpected.EXISTS_CONDITION_REQUIRED);
	}

	/**
	 * Clicks the element using the specified ClickType and then checks that the
	 * expected element meets the expected evaluation criteria
	 * 
	 * @param by
	 * @param type
	 * @param expected
	 * @param eval
	 */
	public void click(By by, ClickType type, By expected, final EvaluateExpected eval) {
		click(findElement(by), by, type, expected, eval);
	}

	/**
	 * Clicks the element using the specified ClickType and then checks that the
	 * expected element meets the expected evaluation criteria
	 * 
	 * @param element
	 * @param type
	 * @param expected
	 * @param eval
	 */
	public void click(WebElement element, ClickType type, By expected, final EvaluateExpected eval) {
		click(element, null, type, expected, eval);
	}

	private void click(WebElement element, By by, ClickType type, By expected, final EvaluateExpected eval) {

		int count = 0;
		while (count <= getDefaultRetryCount()) {

			if (count > 0) {
				log(LogStatus.INFO, "Pre-retry #" + count + " screenshot.");
				takeScreenshot(by, element);
				if (eval.evaluate(getDriver(), expected))
					return;
			}

			if (eval.evaluate(getDriver(), expected) && !eval.name().contains("ALWAYS"))
				return;

			try {
				click(element, by, type, eval);
				getWebDriverWait(getTimeBetweenRetries()).pollingEvery(50, TimeUnit.MILLISECONDS)
						.until((ExpectedCondition<Boolean>) (WebDriver dr) -> eval.evaluate(dr, expected));
				if (getLogging())
					log(LogStatus.INFO,
							"Successfully clicked element " + (by != null ? "<b>" + by.toString() + "</b>." : "."));
				break;
			} catch (Exception e) {

				if ((count + 1) > getDefaultRetryCount()) {
					if (!eval.name().contains("NOT_REQUIRED")) {
						log(LogStatus.FAIL, "Error waiting for expected element state "
								+ (by != null ? "<b>" + expected.toString() + "</b>." : ".") + " Stopping test.");
						takeScreenshot(by, null);
						Assert.fail();
					} else {
						log(LogStatus.INFO,
								"Error waiting for expected element"
										+ (by != null ? " <b>" + by.toString() + "</b>." : ".")
										+ " However, expected element state was not required.  Continuing.");
						break;
					}
				} else {
					log(LogStatus.INFO,
							"Did not find expected element "
									+ (expected != null ? "<b>" + expected.toString() + "</b>." : ".")
									+ " Retrying Click Attempt #" + (count + 1));
					count++;
				}

			}
		}
	}

	/**
	 * Clicks the element using the specified ClickType utilizing the specified
	 * scroll capability
	 * 
	 * @param by
	 * @param type
	 * @param scroll
	 */
	public void click(By by, ClickType type, Scroll scroll) {
		WebElement element = findElement(by);
		scroll.perform(getJsExecutor(), element);
		click(element, type);
	}

	/**
	 * Clicks the element using the specified ClickType utilizing the specified
	 * scroll capability
	 * 
	 * @param element
	 * @param type
	 * @param scroll
	 */
	public void click(WebElement element, ClickType type, Scroll scroll) {
		scroll.perform(getJsExecutor(), element);
		click(element, type);
	}

	/**
	 * Clicks the element using the specified ClickType utilizing the specified
	 * scroll capability and then checks for the presence of the expected
	 * element
	 * 
	 * @param by
	 * @param type
	 * @param scroll
	 * @param expected
	 */
	public void click(By by, ClickType type, Scroll scroll, By expected) {
		WebElement element = findElement(by);
		scroll.perform(getJsExecutor(), element);
		click(by, type, expected);
	}

	/**
	 * Clicks the element using the specified ClickType utilizing the specified
	 * scroll capability and then checks for the presence of the expected
	 * element
	 * 
	 * @param element
	 * @param type
	 * @param scroll
	 * @param expected
	 */
	public void click(WebElement element, ClickType type, Scroll scroll, By expected) {
		scroll.perform(getJsExecutor(), element);
		click(element, type, expected);
	}

	public enum ClickType {

		NORMAL, JAVASCRIPT;

		public boolean perform(JavascriptExecutor jse, WebElement element) {

			switch (this) {
			case JAVASCRIPT:
				jse.executeScript("arguments[0].click();", new Object[] { element });
				return true;
			case NORMAL:
				element.click();
				return true;
			}
			return false;
		}
	}

	public enum Scroll {

		UP, DOWN, NEUTRAL;

		public void perform(JavascriptExecutor jse, WebElement element) {
			switch (this) {
			case NEUTRAL:
				jse.executeScript("arguments[0].scrollIntoView();", new Object[] { element });
				break;
			case DOWN:
				jse.executeScript("arguments[0].scrollIntoView(true);", new Object[] { element });
				break;
			case UP:
				jse.executeScript("arguments[0].scrollIntoView(false);", new Object[] { element });
				break;
			}
		}
	}

	public enum EvaluateExpected {

		EXISTS_CONDITION_NOT_REQUIRED,
		EXISTS_CONDITION_REQUIRED,
		EXISTS_REQUIRED_ALWAYS_TRY_ONCE,
		DOES_NOT_EXIST_REQUIRED_ALWAYS_TRY_ONCE,
		DOES_NOT_EXIST_CONDITION_NOT_REQUIRED,
		DOES_NOT_EXIST_REQUIRED;

		public boolean evaluate(WebDriver dr, By by) {
			switch (this) {
			case EXISTS_CONDITION_NOT_REQUIRED:
			case EXISTS_CONDITION_REQUIRED:
			case EXISTS_REQUIRED_ALWAYS_TRY_ONCE:
				return dr.findElements(by).size() > 0;
			case DOES_NOT_EXIST_REQUIRED_ALWAYS_TRY_ONCE:
			case DOES_NOT_EXIST_CONDITION_NOT_REQUIRED:
			case DOES_NOT_EXIST_REQUIRED:
				return dr.findElements(by).size() == 0;
			}
			return true;
		}

	}

}
