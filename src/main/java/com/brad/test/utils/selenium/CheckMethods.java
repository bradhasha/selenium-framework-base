package com.brad.test.utils.selenium;

import com.brad.test.framework.PortableTestParams;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

public abstract class CheckMethods extends GetMethods {

	public CheckMethods(WebDriver dr, PortableTestParams ptm) {
		super(dr, ptm);
	}

	/**
	 * Compares the element text using selenium's getText() against the specified text for the element against the MatchType condition.
	 * @param element
	 * @param text
	 * @param type
	 * @return evaluation of MatchType condition
	 */
	public boolean checkText(WebElement element, String text, MatchType type) {
		if (text == null)
			return true;
		
		boolean yes = false;
		String elementText = null;

		try {

			elementText = element.getText();
			yes = type.evaluate(elementText, text);

		} catch (Exception e) {
			if (getLogging()) {
				log(LogStatus.FAIL, "Error checking element for text '" + text + "'.  Stopping test.");
				takeScreenshot(null, element);
			}
			Assert.fail(e.getMessage());
		}

		if (getLogging()) {
			log(yes ? LogStatus.INFO : LogStatus.ERROR,
					"Element text " + elementText + " " + (yes ? "did" : "did not") + " " + type.name() + " " + text);
			takeScreenshot(null, element);
		}
		return yes;
	}

	/**
	 * Compares the element text using selenium's getText() against the specified text for the element against the MatchType condition.
	 * @param by
	 * @param text
	 * @param type
	 * @return evaluation of MatchType condition
	 */
	public boolean checkText(By by, String text, MatchType type) {
		if (text == null)
			return true;
		
		WebElement element = null;
		boolean yes = false;
		String elementText = null;

		try {

			element = findElement(by);
			elementText = element.getText();
			yes = type.evaluate(elementText, text);

		} catch (Exception e) {
			if (getLogging()) {
				log(LogStatus.FAIL,
						"Error checking <b>" + by.toString() + "</b> for text '" + text + "'.  Stopping test.");
				takeScreenshot(null, element);
			}
			Assert.fail(e.getMessage());
		}

		if (getLogging()) {
			log(yes ? LogStatus.INFO : LogStatus.FAIL, "Element " + by.toString() + " text " + elementText + " "
					+ (yes ? "did" : "did not") + " " + type.name() + " " + text);
			takeScreenshot(null, element);
			if (!yes)
				Assert.fail();
		}
		return yes;
	}

	/**
	 * Compares the text in an angularJS input object against the specified text for the element against the MatchType condition.
	 * @param by
	 * @param text
	 * @param type
	 * @return evaluation of MatchType condition
	 */
	public boolean checkAngularJsText(By by, String text, MatchType type) {
		if (text == null)
			return true;
		
		boolean yes = false;
		String elementText = null;
		
		try {
			elementText = getAngularJsText(by);
			yes = type.evaluate(elementText, text);

		} catch (Exception e) {
			if (getLogging()) {
				log(LogStatus.FAIL,
						"Error checking <b>" + by.toString() + "</b> for text '" + text + "'.  Stopping test.");
				takeScreenshot(by, null);
			}
			Assert.fail(e.getMessage());
		}

		if (getLogging()) {
			log(yes ? LogStatus.INFO : LogStatus.FAIL, "Element <b>" + by.toString() + "</b> attribute value "
					+ elementText + " <b>" + (yes ? "did" : "did not") + " " + type.name() + "</b> " + text);
			takeScreenshot(by, null);
			if (!yes)
				Assert.fail();
		}
		return yes;
	}

	/**
	 * Compares the value of the specified attribute against the specified text for the element against the MatchType condition.
	 * @param by
	 * @param attr
	 * @param text
	 * @param type
	 * @return evaluation of MatchType condition
	 */
	public boolean checkAttribute(By by, String attr, String text, MatchType type) {
		if (text == null)
			return true;
		
		WebElement element = null;
		boolean yes = false;
		String elementAttrValue = null;

		try {

			element = findElement(by);
			elementAttrValue = element.getAttribute(attr);
			yes = type.evaluate(elementAttrValue, text);

		} catch (Exception e) {
			if (getLogging()) {
				log(LogStatus.FAIL, "Error checking <b>" + by.toString() + "</b> for attribute value '" + text
						+ "'.  Stopping test.");
				takeScreenshot(null, element);
			}
			Assert.fail(e.getMessage());
		}

		if (getLogging()) {
			log(yes ? LogStatus.INFO : LogStatus.ERROR, "Element <b>" + by.toString() + "</b> attribute value "
					+ elementAttrValue + " <b>" + (yes ? "did" : "did not") + " " + type.name() + "</b> " + text);
			takeScreenshot(null, element);
		}
		return yes;
	}

	public enum MatchType {

		CONTAINS, EQUALS, EQUALS_IGNORE_CASE, NOT_EQUAL_IGNORE_CASE, NOT_CONTAINS;

		public boolean evaluate(String text, String expected) {
			if (expected == null)
				return true;
			switch (this) {
			case CONTAINS:
				return text.contains(expected);
			case EQUALS:
				return text.equals(expected);
			case EQUALS_IGNORE_CASE:
				return text.equalsIgnoreCase(expected);
			case NOT_EQUAL_IGNORE_CASE:
				return !text.equalsIgnoreCase(expected);
			case NOT_CONTAINS:
				return !text.contains(expected);
			}
			return false;
		}
	}
}
