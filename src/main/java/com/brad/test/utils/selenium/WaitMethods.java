package com.brad.test.utils.selenium;

import java.util.concurrent.TimeUnit;

import com.brad.test.framework.PortableTestParams;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.testng.Assert;

import com.relevantcodes.extentreports.LogStatus;

public abstract class WaitMethods extends InputMethods {

	public WaitMethods(WebDriver dr, PortableTestParams ptm) {
		super(dr, ptm);
	}

	/**
	 * Wait for the specified element to appear
	 * 
	 * @param by
	 */
	public void waitForElement(By by) {
		waitForElement(by, EvaluateExpected.EXISTS_CONDITION_REQUIRED);
	}

	/**
	 * Wait for the specified element to appear, but control the condition of required/not required.
	 * @param by
	 * @param eval
	 */
	public void waitForElement(By by, EvaluateExpected eval) {
		try {
			getWebDriverWait(getTimeBetweenRetries() * 2).pollingEvery(500, TimeUnit.MILLISECONDS)
					.until((ExpectedCondition<Boolean>) (WebDriver dr) -> {
						return eval.evaluate(dr, by);
					});
		} catch (Exception e) {
			if (!eval.name().contains("NOT_REQUIRED")) {
				log(LogStatus.FAIL, "Error waiting for expected state "
						+ (by != null ? "<b>" + by.toString() + "</b>." : ".") + " Stopping test.");
				takeScreenshot(by, null);
				Assert.fail();
			} else {
				log(LogStatus.INFO,
						"Error waiting for expected element" + (by != null ? " <b>" + by.toString() + "</b>." : ".")
								+ " However, expected element state was not required.  Continuing.");
			}
		}
	}

	/**
	 * Wait for the specified text in the specified element to meet the
	 * MatchType condition
	 * 
	 * @param by
	 * @param expectedValue
	 * @param type
	 */
	public void waitForText(By by, String expectedValue, MatchType... type) {
		checkMatchTypeCount(type);
		waitForText(null, by, expectedValue, type.length == 0 ? MatchType.EQUALS : type[0]);
	}

	/**
	 * Wait for the specified text in the specified element to meet the
	 * MatchType condition
	 * 
	 * @param element
	 * @param expectedValue
	 * @param type
	 */
	public void waitForText(WebElement element, String expectedValue, MatchType... type) {
		checkMatchTypeCount(type);
		waitForText(element, null, expectedValue, type.length == 0 ? MatchType.EQUALS : type[0]);
	}

	/**
	 * Wait for the specified attribute's value in the specified element to meet
	 * the MatchType condition
	 * 
	 * @param by
	 * @param attr
	 * @param expectedValue
	 * @param type
	 */
	public void waitForAttributeValue(By by, String attr, String expectedValue, MatchType... type) {
		checkMatchTypeCount(type);
		waitForAttributeValue(null, by, attr, expectedValue, type.length == 0 ? MatchType.EQUALS : type[0]);
	}

	/**
	 * Wait for the specified attribute's value in the specified element to meet
	 * the MatchType condition
	 * 
	 * @param element
	 * @param attr
	 * @param expectedValue
	 * @param type
	 */
	public void waitForAttributeValue(WebElement element, String attr, String expectedValue, MatchType... type) {
		checkMatchTypeCount(type);
		waitForAttributeValue(element, null, attr, expectedValue, type.length == 0 ? MatchType.EQUALS : type[0]);
	}

	private void waitForAttributeValue(WebElement element, By by, String attr, String expectedValue, MatchType type) {
		getWebDriverWait(getTimeBetweenRetries() * 2).pollingEvery(1, TimeUnit.SECONDS)
				.until((ExpectedCondition<Boolean>) (WebDriver dr) -> {
					try {
						String value = element == null ? findElement(by).getAttribute(attr)
								: element.getAttribute(attr);
						return expectedValue == null ? value == null : type.evaluate(value, expectedValue);
					} catch (Exception e) {

					}
					return false;
				});
		log(LogStatus.INFO, "Succesfully located text " + expectedValue + " using selenium getAttribute() in element "
				+ by.toString());
	}

	private void waitForText(WebElement element, By by, String expectedValue, MatchType type) {
		getWebDriverWait(getTimeBetweenRetries() * 2).pollingEvery(1, TimeUnit.SECONDS)
				.until((ExpectedCondition<Boolean>) (WebDriver dr) -> {
					try {
						String value = element == null ? findElement(by).getText() : element.getText();
						return expectedValue == null ? value == null : type.evaluate(value, expectedValue);
					} catch (Exception e) {

					}
					return false;
				});
		log(LogStatus.INFO,
				"Succesfully located text " + expectedValue + " using selenium getText() in element " + by.toString());
	}

	/**
	 * Wait for the specified text to exist in the specified angularJS element
	 * 
	 * @param by
	 * @param expectedValue
	 */
	public void waitForAngularJsText(By by, String expectedValue) {
		waitForAngularJsText(by, expectedValue, MatchType.EQUALS);
	}

	/**
	 * Wait for the specified text in the specified angularJS element to meet
	 * the MatchType condition
	 * 
	 * @param by
	 * @param expectedValue
	 * @param type
	 */
	public void waitForAngularJsText(By by, String expectedValue, MatchType type) {
		getWebDriverWait(getTimeBetweenRetries() * 2).pollingEvery(1, TimeUnit.SECONDS)
				.until((ExpectedCondition<Boolean>) (WebDriver dr) -> {
					String value = getAngularJsText(by);
					return expectedValue == null ? value == null : type.evaluate(value, expectedValue);
				});
		log(LogStatus.INFO,
				"Succesfully located text " + expectedValue + " using angular js object in element " + by.toString());
	}

	private void checkMatchTypeCount(MatchType... type) {
		if (type.length > 1) {
			log(LogStatus.ERROR, "You may only pass in one MatchType.");
			Assert.fail();
		}
	}

}
