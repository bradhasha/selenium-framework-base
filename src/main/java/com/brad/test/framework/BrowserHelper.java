/**
 * BrowserHelper.java
 * 
 * @author bhasha 2016
 */
package com.brad.test.framework;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class that is used to validate and specify browser specficiations for test
 * cases eg. browser, version, platform, profile
 */
public class BrowserHelper {

	/**
	 * Enum to Store Property Types and IF they are required
	 * 
	 * @author bhasha
	 *
	 */
	private enum Requirement {

		BROWSER(true), VERSION(false), PLATFORM(false), PROFILEDIR(false), ISPROFILEDIRREMOTE(false);

		private boolean needed = false;

		private Requirement(final boolean needed) {
			this.needed = needed;
		}

		/**
		 * Checks to see if property type is mandatory
		 * 
		 * @return true if property is mandatory
		 */
		public boolean met() {
			return this.needed;
		}

		/**
		 * Validates input for each property type
		 * 
		 * @param input
		 * @return true if input passes regex test
		 */
		public boolean isValid(String input) {
			boolean passed = false;
			Pattern pattern = null;
			Matcher matcher = null;

			switch (this.name().toUpperCase()) {
			case "BROWSER":
			case "PLATFORM":
				pattern = Pattern.compile("(^[A-Z]+$)");
				matcher = pattern.matcher(input);
				passed = matcher.matches();
				break;
			case "VERSION":
				// TODO: Figure out how to determine if the string matches the
				// jsemver External DSL.
				passed = true;
				break;
			case "PROFILEDIR":
				// TODO: Reject invalid file paths (either don't exist or the
				// file path uses unallowable characters)
				passed = true;
				break;
			case "ISPROFILEDIRREMOTE":
				try {
					Boolean.parseBoolean(input);
					passed = true;
				} catch (ClassCastException cce) {
					passed = false;
				}
				break;
			default:
				throw new RuntimeException("Unknown property specified: " + this.name());
			}
			return passed;
		}
	}

	@SuppressWarnings("unused")
	private static class MutableBrowserInfo extends BaseBrowserInfo {

		public MutableBrowserInfo() {
			super(null, null, null, null, false);
		}

		public MutableBrowserInfo(String b, String v, String p, String d, boolean i) {
			super(b, v, p, d, i);
		}

		public void setBrowser(String browser) {
			this.b = browser;
		}

		public void setVersion(String version) {
			this.v = version;
		}

		public void setPlatform(String platform) {
			this.p = platform;
		}

		public void setProfileDir(String d) {
			this.d = d;
		}

		public void setIsProfileDirRemote(boolean b) {
			this.i = b;
		}

	}

	// Stores properties and their values as passed in by application.browsers
	private final List<BrowserInfo> propsList = new ArrayList<BrowserInfo>();
	private final List<String> baseUrls = new ArrayList<>();

	/**
	 * Constructor
	 * 
	 * @param values
	 *            - application.browsers string extracted from java system
	 *            property
	 */
	public BrowserHelper(String values) {

		String[] browsers = values != null && values.trim().length() > 0 ? values.split(",") : null;

		if (browsers != null)
			for (String b : browsers) {
				propsList.add(parseBrowserString(b));
			}
	}

	private BrowserInfo parseBrowserString(String values) {
		// Split properties by | character
		String[] selected = values.split("\\|");

		HashMap<String, String> props = new HashMap<String, String>();

		if (values.equals("DEFAULT")) {
			props.put(Requirement.BROWSER.name(), "FIREFOX");
		} else {
			// Checks to see if there is more than one property
			if (selected.length > 0) {

				// Loop through properties and separate property names from
				// their values
				for (String select : selected) {
					String[] curr = select.split("=");

					// Ensure property value was declared correctly, = character
					// is required
					if (curr.length < 2)
						throw new RuntimeException(
								"Error parsing properties.  You must declare properties like this: PROPERTYNAME=PROPERTYVALUE|PROPERTYNAME=PROPERTYVALUE");

					// First we have to combine any additional parts after the
					// first equals sign
					// This supports having equals signs in the value.
					StringBuilder sb = new StringBuilder(curr[1]);
					for (int i = 2; i < curr.length; i++) {
						sb.append("=");
						sb.append(curr[i]);
					}

					// Add property name and its value to the hashmap
					String propKey = curr[0].toUpperCase().trim(), propVal = sb.toString().trim();
					if (propKey.equals("PROFILEDIR") || propKey.equals("ISPROFILEDIRREMOTE"))
						props.put(propKey, propVal);
					else
						props.put(propKey, propVal.toUpperCase());
				}
			} else {

				props.put(Requirement.BROWSER.name(), "FIREFOX");
			}
		}

		// Validates the user declared the required properties, and only
		// declared allowed properties
		if (!hasValidProperties(props))
			throw new RuntimeException(
					"You did not declare required properties, or declared invalid properties.\nRequired properties include: "
							+ getRequiredProperties() + "\nOptional Properties include: " + getOptionalProperties()
							+ "\nSeparate multiple properties with the '|' character. Eg. PROPERTYNAME=PROPERTYVALUE|PROPERTYNAME=PROPERTYVALUE");

		validatePropertyValues(props);
		return getInfoFromMap(props);
	}

	private BrowserInfo getInfoFromMap(Map<String, String> props) {
		String browser = props.get(Requirement.BROWSER.name());
		String version = props.get(Requirement.VERSION.name());
		String platform = props.get(Requirement.PLATFORM.name());
		String profileDir = props.get(Requirement.PROFILEDIR.name());
		boolean isProfileDirRemote = Boolean.parseBoolean(props.get(Requirement.ISPROFILEDIRREMOTE.name()));
		return new MutableBrowserInfo(browser, version, platform, profileDir, isProfileDirRemote);
	}

	public List<BrowserInfo> getBrowserInfos() {
		return Collections.unmodifiableList(propsList);
	}

	/**
	 * Verifies user declared required properties, and only declared allowed
	 * properties
	 * 
	 * @return true if user entered input correctly
	 */
	private boolean hasValidProperties(Map<String, String> props) {
		int met = 0;
		int valid = props.size();
		int checked = 0;
		for (Object prop : props.keySet().toArray()) {

			for (Requirement req : Requirement.values()) {
				if (prop.toString().equals(req.name())) {
					checked++;
					if (req.met())
						met++;
				}
			}
		}
		return met == getRequiredNeeded() && checked == valid;
	}

	/**
	 * Validates that property values match their allowed format using regex
	 * 
	 * @return true if user entered data correctly
	 */
	private void validatePropertyValues(Map<String, String> props) {
		for (Object prop : props.keySet().toArray()) {

			for (Requirement req : Requirement.values()) {
				if (prop.toString().equals(req.name())) {
					if (!req.isValid(props.get(prop.toString())))
						throw new RuntimeException("Property " + prop.toString() + " with value "
								+ props.get(prop.toString()) + " is invalid.");
				}
			}
		}
	}

	/**
	 * Get required properties
	 * 
	 * @return string of all properties that are required separated by a |
	 */
	public String getRequiredProperties() {
		StringBuilder sb = new StringBuilder();
		for (Requirement requirement : Requirement.values())
			if (requirement.met())
				sb.append(requirement.name() + (getRequiredNeeded() > 1 ? "|" : ""));
		return sb.toString();
	}

	/**
	 * Get optional properties
	 * 
	 * @return string of all properties that are required separated by a |
	 */
	public String getOptionalProperties() {
		StringBuilder sb = new StringBuilder();
		for (Requirement requirement : Requirement.values())
			if (!requirement.met())
				sb.append(requirement.name() + ",");
		return sb.toString();
	}

	/**
	 * Get the amount of required properties
	 * 
	 * @return count of properties that are required
	 */
	public int getRequiredNeeded() {
		int req = 0;
		for (Requirement requirement : Requirement.values()) {
			if (requirement.met())
				req++;
		}
		return req;
	}

	public static BrowserHelper getDefaultHelper() {

		String applicationUrls = System.getProperty("application.urls");
		String applicationBrowsers = System.getProperty("application.browsers");
		String[] urlsList = safeCommaSplit(applicationUrls);
		if (urlsList.length == 0)
			urlsList = safeCommaSplit(System.getProperty("application.urls"));

		BrowserHelper retval = new BrowserHelper(applicationBrowsers);

		Collections.addAll(retval.baseUrls, urlsList);

		System.out.println("URLs:" + Arrays.toString(urlsList));

		return retval;
	}

	public static String[] safeCommaSplit(String s) {
		if (s == null || s.trim().length() == 0) {
			return new String[0];
		} else {
			return s.split(",");
		}
	}

	public List<String> getURLs() {
		return baseUrls;
	}

}