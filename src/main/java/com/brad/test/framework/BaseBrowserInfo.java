/**
 * BaseBrowserInfo.java
 * 
 * @author sean 2016
 */
package com.brad.test.framework;

/**
 * BaseBrowserInfo class for returning the browser info that is collected.
 */
public class BaseBrowserInfo implements BrowserInfo {
	protected String b, v, p, d;
	protected boolean i;

	public BaseBrowserInfo(String b, String v, String p, String d, boolean i) {
		this.b = b;
		this.v = v;
		this.p = p;
		this.d = d;
		this.i = i;
	}

	public BaseBrowserInfo(String b) {
		this(b, null, null, null, false);
	}

	@Override
	/**
	 * obtains the browser
	 */
	public String getBrowser() {
		return b;
	}

	@Override
	/**
	 * Obtains the browser version
	 */
	public String getVersion() {
		return v;
	}

	@Override
	/**
	 * Obtains the Platform that the browser is running on.
	 */
	public String getPlatform() {
		return p;
	}

	@Override
	/**
	 * Obtain the Profile Directory
	 */
	public String getProfileDir() {
		return d;
	}

	@Override
	/**
	 * Returns all of the Browser info in a string
	 */
	public String toString() {
		return getClass().getName() + " [Browser=" + getBrowser() + "|Version=" + getVersion() + "|Platform="
				+ getPlatform() + "|ProfileDir=" + getProfileDir() + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((b == null) ? 0 : b.hashCode());
		result = prime * result + ((d == null) ? 0 : d.hashCode());
		result = prime * result + ((p == null) ? 0 : p.hashCode());
		result = prime * result + ((v == null) ? 0 : v.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BaseBrowserInfo other = (BaseBrowserInfo) obj;
		if (b == null) {
			if (other.b != null)
				return false;
		} else if (!b.equals(other.b))
			return false;
		if (d == null) {
			if (other.d != null)
				return false;
		} else if (!d.equals(other.d))
			return false;
		if (p == null) {
			if (other.p != null)
				return false;
		} else if (!p.equals(other.p))
			return false;
		if (v == null) {
			if (other.v != null)
				return false;
		} else if (!v.equals(other.v))
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see BrowserInfo#isProfileDirRemote()
	 */
	@Override
	public boolean isProfileDirRemote() {
		return i;
	}

}