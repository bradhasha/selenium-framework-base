/**
 * PortableTestParams.java
 * 
 * @author sean 2016
 */
package com.brad.test.framework;

import java.util.ArrayList;
import java.util.List;

public class PortableTestParams {

	private String method;
	private BrowserInfo browserInfo;
	private String url;

	public PortableTestParams(String method, BrowserInfo browserInfo, String url) {
		this.method = method;
		this.browserInfo = browserInfo;
		this.url = url;
	}

	@SuppressWarnings("unused")
	private PortableTestParams() {
	}

	public void copyInto(PortableTestParams ptm) {
		this.method = ptm.getMethod();
		this.browserInfo = ptm.getBrowserInfo();
		this.url = ptm.getUrl();
	}

	/**
	 * @return the method
	 */
	public String getMethod() {
		return method;
	}

	/**
	 * @return the browserInfo
	 */
	public BrowserInfo getBrowserInfo() {
		return browserInfo;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param o
	 *            array of test param data
	 * @return list of PortableTestParams objects
	 */
	public static final List<PortableTestParams> toList(Object[][] o) {
		ArrayList<PortableTestParams> ptm = new ArrayList<PortableTestParams>();
		for (Object[] oo : o) {
			ptm.add((PortableTestParams) oo[0]);
		}
		return ptm;
	}

	/**
	 * @param ptm
	 *            List of PortableTestParams objects
	 * @return object array of PortableTestParams from list
	 */
	public static final Object[][] toArray(List<PortableTestParams> ptm) {
		if (ptm == null)
			return null;
		Object[][] retval = new Object[ptm.size()][];
		int i = 0;
		for (PortableTestParams p : ptm) {
			retval[i++] = new Object[] { p };
		}

		return retval;
	}

	@Override
	public String toString() {
		return "PortableTestParams: Method: " + getMethod() + " | BrowserInfo: " + getBrowserInfo().toString()
				+ " | URL: " + getUrl();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((browserInfo == null) ? 0 : browserInfo.hashCode());
		result = prime * result + ((method == null) ? 0 : method.hashCode());
		result = prime * result + ((url == null) ? 0 : url.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PortableTestParams other = (PortableTestParams) obj;
		if (browserInfo == null) {
			if (other.browserInfo != null)
				return false;
		} else if (!browserInfo.equals(other.browserInfo))
			return false;
		if (method == null) {
			if (other.method != null)
				return false;
		} else if (!method.equals(other.method))
			return false;
		if (url == null) {
			if (other.url != null)
				return false;
		} else if (!url.equals(other.url))
			return false;
		return true;
	}

}