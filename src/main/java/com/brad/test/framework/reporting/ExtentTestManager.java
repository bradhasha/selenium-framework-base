package com.brad.test.framework.reporting;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

public class ExtentTestManager {

	static Map<ExtentTest, ArrayList<File>> screenshots = new HashMap<ExtentTest, ArrayList<File>>();
	static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
	private static ExtentReports extent = ExtentManager.getInstance();

	public static synchronized void addScreenshot(File file) {
		ArrayList<File> files = getScreenshots(getTest()) != null ? getScreenshots(getTest()) : new ArrayList<File>();
		files.add(file);

		screenshots.put(getTest(), files);
	}

	public static synchronized ArrayList<File> getScreenshots(ExtentTest test) {
		return screenshots.get(test);
	}

	/**
	 * Get current test by thread id
	 * 
	 * @return current test
	 */
	public static synchronized ExtentTest getTest() {
		return extentTestMap.get((int) (long) (Thread.currentThread().getId()));
	}

	/**
	 * End current test by thread id
	 */
	public static synchronized void endTest() {
		extent.endTest(extentTestMap.get((int) (long) (Thread.currentThread().getId())));
	}

	/**
	 * Start test by test method name
	 * 
	 * @param testName
	 *            the name of the test to start
	 * @return new instance of the test
	 */
	public static synchronized ExtentTest startTest(String testName) {
		return startTest(testName, "");
	}

	/**
	 * Add new test to map
	 * 
	 * @param testName
	 * @param desc
	 * @return new instance of the test
	 */
	public static synchronized ExtentTest startTest(String testName, String desc) {
		ExtentTest test = extent.startTest(testName, desc);
		extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
		return test;
	}
}