package com.brad.test.framework.reporting;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

import javax.imageio.ImageIO;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Rectangle;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ExtentReportUtils {

	/**
	 * Basic log for instances where this may be needed
	 * 
	 * @param logStatus
	 *            the type of log message to log
	 * @param msg
	 *            The message to log
	 */
	public static void log(LogStatus logStatus, String msg) {
		boolean useExtentLogger = Boolean.valueOf(System.getProperty("useExtentLogger"));
		if (useExtentLogger && msg.length() < 1000)
			getTest().log(logStatus, msg);
		System.out.println("Normal Logger - " + logStatus.name() + ": " + msg);
	}

	private static BufferedImage fillTransparentPixels(BufferedImage image, Color fillColor) {
		int w = image.getWidth();
		int h = image.getHeight();
		BufferedImage image2 = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = image2.createGraphics();
		g.setColor(fillColor);
		g.fillRect(0, 0, w, h);
		g.drawRenderedImage(image, null);
		g.dispose();
		return image2;
	}

	/**
	 * Use webdriver instance to take screenshot and insert into log file of
	 * test if takeScreenshots is set to true
	 * 
	 * @param dr
	 *            the current webdriver object being used
	 * @param by
	 *            the element selector
	 * @param rect
	 *            the rectangle of the WebElement. Passed in by
	 *            WebElement.getRectangle();
	 * @throws IOException
	 *             catch io exception
	 */
	public static void takeAndInsertScreenshot(WebDriver dr, By by, Rectangle rect) {
		try {

			final boolean takeScreenshots = Boolean.valueOf(System.getProperty("takeScreenshots"));
			if (takeScreenshots) {

				final boolean drawRectangle = Boolean
						.valueOf(System.getProperty("drawRectangleAroundElementsWithScreenshots"));
				final boolean drawElementSelector = Boolean
						.valueOf(System.getProperty("drawElementSelectorAboveElementsWithScreenshots"));

				// Take screenshot
				File ss = ((TakesScreenshot) dr).getScreenshotAs(OutputType.FILE);
				BufferedImage bfImg = fillTransparentPixels(ImageIO.read(ss), Color.white);

				// Draw on picture if applicable
				if (rect != null && (drawRectangle || drawElementSelector)) {

					Graphics2D gfx = bfImg.createGraphics();
					gfx.setColor(Color.RED);
					gfx.setStroke(new BasicStroke(2));
					gfx.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
					gfx.setFont(new Font("Serif", Font.BOLD, 16));

					// Draw selector string if applicable
					if (by != null && drawElementSelector)
						gfx.drawString(by.toString(), rect.getX(), rect.getY() - 15);

					// Draw rectangle around element if applicable
					if (drawRectangle)
						gfx.drawRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());

					gfx.dispose();
				}

				// Save image as jpeg
				ss = new File(ss.getName().replaceAll("png", "jpg"));
				ImageIO.write(bfImg, "jpg", ss);

				// Place ss into report temp dir
				File dir = new File(ExtentManager.getReportImageDirectory());
				ExtentTestManager.addScreenshot(new File(ExtentManager.getReportImageDirectory() + ss.getName()));
				FileUtils.copyFileToDirectory(ss, dir);
				FileUtils.deleteQuietly(ss);

				// Add screenshot to log file
				getTest().log(LogStatus.INFO, "", ExtentReportUtils.getTest()
						.addScreenCapture(ExtentManager.getRelativeReportImageDirectory() + ss.getName()));

			}
		} catch (Exception e) {
			System.out.println("Error with taking screenshot.");
		}
	}

	/**
	 * Return current instance of ExtentTest
	 * 
	 * @return current instance of ExtentTest
	 */
	public static synchronized ExtentTest getTest() {
		return ExtentTestManager.getTest();
	}

	public static synchronized void writeEnvironmentVariableFile()
			throws FileNotFoundException, UnsupportedEncodingException {
		final String failedTests = System.getProperty("failedTests") == null ? "" : System.getProperty("failedTests");
		File envVariablesFile = new File(ExtentManager.getReportDirectory() + "/envVariables.properties");
		PrintWriter pw = new PrintWriter(envVariablesFile, "UTF-8");
		pw.println("failedTests=" + failedTests.replaceAll(",$", ""));
		pw.close();
	}

}
