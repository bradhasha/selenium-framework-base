package com.brad.test.framework.reporting;

import java.nio.file.Paths;

import org.testng.Reporter;

import com.relevantcodes.extentreports.ExtentReports;

public class ExtentManager {
	private static ExtentReports extent;

	/**
	 * Get instance of ExtentReports
	 * 
	 * @return instance of ExtentReports
	 */
	public synchronized static ExtentReports getInstance() {
		if (extent == null) {

			extent = new ExtentReports(getReportDirectory() + "/Report.html", true);
			Reporter.log("Extent Report directory: " + getReportDirectory(), true);

		}
		return extent;
	}

	private static String determineDir() {
		String jenkinsBuild = System.getProperty("BUILD_TAG");
		return Paths.get(".").toAbsolutePath().normalize().toString() + "/Extent Reports"
				+ (jenkinsBuild != null && jenkinsBuild.length() > 0 ? "/" + jenkinsBuild : "/default");
	}

	public static String getReportDirectory() {
		return determineDir();
	}

	public static String getRelativeReportImageDirectory() {
		return "./temp/";
	}

	public static String getReportImageDirectory() {
		return determineDir() + "/temp/";
	}
}