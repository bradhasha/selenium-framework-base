package com.brad.test.framework.managers;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.brad.test.framework.BrowserHelper;
import com.brad.test.framework.PortableTestParams;
import com.brad.test.framework.reporting.ExtentManager;
import com.brad.test.framework.reporting.ExtentReportUtils;
import com.brad.test.framework.reporting.ExtentTestManager;
import com.brad.test.framework.reporting.TestRecorder;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import com.brad.test.framework.BrowserInfo;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.model.Log;

public abstract class TestManager {

	// Drivers
	protected final ConcurrentMap<String, WebDriver> currentDrivers = new ConcurrentHashMap<>();
	protected final ArrayList<String> baseUrls = new ArrayList<>();
	protected BrowserHelper browserHelper = null;

	// Reporting
	public static ExtentReports extent;

	// Test logging features
	private TestRecorder tr = null;
	private boolean takeScreenRecording = false;

	// Abstract methods
	public abstract void test(PortableTestParams ptm) throws IOException, InterruptedException;

	// Constructors
	public TestManager(String applicationUrls, String applicationBrowsers) {
		System.err.println("New TestManager of child class named " + this.getClass().getName());
		setupApplicationSettings(applicationUrls, applicationBrowsers);
	}

	public TestManager(String applicationUrls) {
		this(applicationUrls, System.getProperty("application.browsers"));
	}

	public TestManager() {
		this(System.getProperty("application.urls"), System.getProperty("application.browsers"));
	}

	/**
	 * Initialize driverTypes based off System Property list
	 * 
	 * @param applicationUrls
	 *            comma separated list of urls to test against
	 * @param applicationBrowsers
	 *            comma separated list of browsers to test against
	 */
	private void setupApplicationSettings(String applicationUrls, String applicationBrowsers) {

		browserHelper = BrowserHelper.getDefaultHelper();
		baseUrls.addAll(browserHelper.getURLs());
		System.out.println("URLs:" + Arrays.deepToString(baseUrls.toArray()));

		this.takeScreenRecording = Boolean.valueOf(System.getProperty("takeScreenRecording"));
	}

	/**
	 * Instantiates a new driver according to the driverName passed in through
	 * the constructor.
	 * 
	 * @return new instantiated driver
	 * @throws IOException
	 */
	protected WebDriver setupDriver(final String driverName) throws IOException {
		WebDriver dr = null;
		DesiredCapabilities dc = null;

		boolean useSeleniumGrid = Boolean.valueOf(System.getProperty("useSeleniumGrid"));
		final String seleniumGridUrl = System.getProperty("seleniumGridUrl");

		switch (driverName.trim().toUpperCase()) {
		case "FIREFOX":
			FirefoxProfile fp = null;
			dc = DesiredCapabilities.firefox();
			final String fpProfile = System.getProperty("firefoxProfilePath");
			final boolean useFpProfile = Boolean.valueOf(System.getProperty("useSpecifiedFirefoxProfile"));
			if (useFpProfile && fpProfile != null && fpProfile.length() > 0) {
				fp = new FirefoxProfile(new File(fpProfile));
				dc.setCapability(FirefoxDriver.PROFILE, fp);
			} else {
				fp = new FirefoxProfile();
			}
			if (useSeleniumGrid)
				dr = new RemoteWebDriver(new URL(seleniumGridUrl), dc);
			else
				dr = new FirefoxDriver(dc);
			break;

		case "IE":
			dc = DesiredCapabilities.internetExplorer();
			dc.setCapability(InternetExplorerDriver.IGNORE_ZOOM_SETTING, true);
			dc.setCapability(InternetExplorerDriver.NATIVE_EVENTS, false);
			dc.setCapability(InternetExplorerDriver.UNEXPECTED_ALERT_BEHAVIOR, true);
			dc.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
			dc.setCapability(InternetExplorerDriver.ENABLE_PERSISTENT_HOVERING, true);
			if (useSeleniumGrid)
				dr = new RemoteWebDriver(new URL(seleniumGridUrl), dc);
			else
				dr = new InternetExplorerDriver(dc);
			break;

		case "CHROME":
			dc = DesiredCapabilities.chrome();
			if (useSeleniumGrid)
				dr = new RemoteWebDriver(new URL(seleniumGridUrl), dc);
			else
				dr = new ChromeDriver(dc);
			break;

		}

		if (dr != null) {
			final String defaultPageLoadTimeout = System.getProperty("defaultPageLoadTimeout");
			if (defaultPageLoadTimeout != null && defaultPageLoadTimeout.length() > 0) {
				try {
					dr.manage().timeouts().pageLoadTimeout(Integer.valueOf(defaultPageLoadTimeout), TimeUnit.SECONDS);
				} catch (Exception e) {
					System.out.println("The value for defaultPageLoadTimeout must be an integer value only.");
				}
			} else {
				throw new RuntimeException(
						"You must declare java system variables in your pom.xml file.  <defaultPageLoadTimeout>timeoutinseconds</defaultPageLoadTimeout>");
			}
			dr.manage().window().maximize();
		}

		if (dr == null)
			throw new RuntimeException("setupDriver() failed to initialize a valid driver");
		return dr;
	}

	/**
	 * Return web driver matching the PortableTestParams
	 * 
	 * @return web driver if exist otherwise will return null
	 */
	public WebDriver getCurrentDriver(PortableTestParams ptm) {
		return currentDrivers.get(ptm.getMethod() + "_" + ptm.getBrowserInfo().getBrowser());
	}

	@DataProvider(parallel = true)
	public Object[][] dataProvider(Method m) {
		Object[][] retval = new Object[browserHelper.getBrowserInfos().size() * baseUrls.size()][];
		int i = 0;
		String mm = m.toString();
		for (BrowserInfo o : browserHelper.getBrowserInfos()) {
			for (String s : baseUrls) {
				retval[i++] = new Object[] { new PortableTestParams(mm, o, s) };
			}
		}

		return retval;
	}

	@BeforeSuite(alwaysRun = true)
	public void extentSetup(ITestContext context) throws IOException {
		extent = ExtentManager.getInstance();
		extent.loadConfig(new File("extent-config.xml"));

		boolean clearTemp = Boolean.valueOf(System.getProperty("clearExtentReportsTempFolderOnRun"));
		if (clearTemp) {
			File dir = new File(ExtentManager.getReportImageDirectory());
			if (dir.exists()) {
				FileUtils.cleanDirectory(dir);
				System.out.println("INFO: Cleaned old report files from: " + dir.getAbsolutePath());
			}
		}
	}

	@BeforeMethod(alwaysRun = true)
	public void startExtent(Method method, Object[] data) throws Exception {
		ExtentTestManager.startTest(method.getName());
		ExtentReportUtils.log(LogStatus.INFO, "Test Started - " + method.getName());

		PortableTestParams ptm = (PortableTestParams) data[0];
		if (ptm == null)
			throw new RuntimeException("PortableTestParams is null, unable to continue.");

		final String key = method.toString() + "_" + ptm.getBrowserInfo().getBrowser();
		WebDriver value = setupDriver(ptm.getBrowserInfo().getBrowser());
		currentDrivers.put(key, value);
		// System.err.println("Putting " + wd.toString() + " in as the driver
		// for " + method.toString());

		if (takeScreenRecording) {
			System.out.println("INFO: This session will be recorded.  takeScreenRecording is set to true in pom.xml");
			tr = new TestRecorder(method.getName());
			tr.start();
		}

	}

	@AfterMethod(alwaysRun = true)
	public void afterEachTestMethod(ITestResult result) throws IOException {

		ExtentTest test = ExtentReportUtils.getTest();
		test.setStartedTime(getTime(result.getStartMillis()));
		test.setEndedTime(getTime(result.getEndMillis()));

		if (tr != null) {
			tr.stop();
			test.log(LogStatus.INFO, "Video Recording",
					ExtentReportUtils.getTest().addScreencast("temp/" + tr.getResultName()));
		}

		for (String group : result.getMethod().getGroups())
			test.assignCategory(group);

		switch (result.getStatus()) {
		case ITestResult.SUCCESS:
			ExtentReportUtils.log(LogStatus.PASS, "Test Passed: " + ExtentReportUtils.getTest().getTest().getName());

			// If deleteScreenshotsOnPass then remove instances from test log of
			// screenshots and delete the screenshots created by the test.
			final boolean deleteScreenshotsOnPass = Boolean.valueOf(System.getProperty("deleteScreenshotsOnPass"));
			if (deleteScreenshotsOnPass) {
				List<Log> ll = test.getTest().getLogList();
				ll.removeIf(i -> i.getDetails().contains(".jpg"));
				test.getTest().setLog(ll);

				for (File tFile : ExtentTestManager.getScreenshots(test))
					FileUtils.deleteQuietly(tFile);
			}
			break;

		case ITestResult.SKIP:
			ExtentReportUtils.log(LogStatus.SKIP, "Exception" + result.getThrowable().toString());
			ExtentReportUtils.log(LogStatus.SKIP, "Test Skipped: " + test.getTest().getName());
			break;

		case ITestResult.FAILURE:
			ExtentReportUtils.log(LogStatus.FAIL, "Test Failed: " + test.getTest().getName());
			final String failedTests = System.getProperty("failedTests") == null ? ""
					: System.getProperty("failedTests");
			/*System.setProperty("failedTests",
					failedTests + test.getTest().getCategoryList().stream()
							.filter(next -> next.getName().matches("AT-[0-9]{2}\\.[0-9]{2}")).findFirst().get()
							.getName() + ",");*/
			break;

		default:
			ExtentReportUtils.log(LogStatus.ERROR, "Test Passed w/ Errors: " + test.getTest().getName());
			break;
		}
		ExtentTestManager.endTest();
		extent.flush();
	}

	/**
	 * Kill all web driver processes after tests complete
	 * 
	 * @throws Exception
	 */
	@AfterClass(alwaysRun = true)
	public void tearDown() throws Exception {
		if (currentDrivers != null)
			for (String s : currentDrivers.keySet()) {
				WebDriver driver = currentDrivers.get(s);
				if (driver != null)
					driver.quit();
			}
		ExtentReportUtils.writeEnvironmentVariableFile();
	}

	/**
	 * Specify details about the test
	 * 
	 * @param name
	 * @param description
	 * @param categories
	 */
	protected void setTestDetails(String name, String description, String... categories) {
		ExtentReportUtils.getTest().getTest().setName(name);
		ExtentReportUtils.getTest().assignCategory(categories);
		ExtentReportUtils.log(LogStatus.INFO, description);
	}

	/**
	 * Get current date time
	 * 
	 * @param millis
	 * @return Date
	 */
	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	}

}
