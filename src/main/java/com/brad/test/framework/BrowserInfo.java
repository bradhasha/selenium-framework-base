/**
 * BrowserInfo.java
 * 
 * @author sean 2016
 */
package com.brad.test.framework;

public interface BrowserInfo {

  /**
   * @return the browser
   */
  public String getBrowser();

  /**
   * @return the version
   */
  public String getVersion();

  /**
   * @return the platform
   */
  public String getPlatform();

  /**
   * @return the browser profile
   */
  public String getProfileDir();

  /**
   * 
   * @return Whether the profile dir is local (false) or remote (true). If local, attempt to upload the profile to the
   *         remote box (in a grid configuration) before using it. If remote, just try to use the profile dir directly.
   */
  public boolean isProfileDirRemote();

  @Override
  public int hashCode();

  @Override
  public boolean equals(Object o);
}